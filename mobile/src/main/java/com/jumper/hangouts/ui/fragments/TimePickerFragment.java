package com.jumper.hangouts.ui.fragments;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.widget.TimePicker;

import com.jumper.hangouts.R;

import java.util.Calendar;

/**
 * Created on 26/01/16.
 *
 * @author Meeth D Jain
 */
public class TimePickerFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener {

    public static final String ARG_IS_24_HOUR_VIEW = "arg_is_24_hour_view";
    public static final String ARG_HOUR_OF_DAY = "arg_hour_of_day";
    public static final String ARG_MINUTE = "arg_minute";

    private OnTimeSelectedListener mOnTimeSelectedListener;

    public static String getMyTag() {
        return TimePickerFragment.class.getSimpleName();
    }

    public static TimePickerFragment newInstance(Bundle bundle, OnTimeSelectedListener onTimeSelectedListener) {
        TimePickerFragment fragment = new TimePickerFragment();
        fragment.mOnTimeSelectedListener = onTimeSelectedListener;
        fragment.setArguments(bundle);
        return fragment;
    }

    public static Bundle getBundle(int hourOfDay, int minute, boolean is24HourView) {
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_HOUR_OF_DAY, hourOfDay);
        bundle.putInt(ARG_MINUTE, minute);
        bundle.putBoolean(ARG_IS_24_HOUR_VIEW, is24HourView);
        return bundle;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar calendar = Calendar.getInstance();

        boolean is24HourView = false;
        int hourOfDay = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);

        Bundle bundle = getArguments();
        if (bundle != null) {
            is24HourView = bundle.getBoolean(ARG_IS_24_HOUR_VIEW, false);
            hourOfDay = bundle.getInt(ARG_HOUR_OF_DAY, hourOfDay);
            minute = bundle.getInt(ARG_MINUTE, minute);
        }

        return new TimePickerDialog(getActivity(), R.style.DialogTheme, this, hourOfDay, minute, is24HourView);
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        if (mOnTimeSelectedListener != null) {
            mOnTimeSelectedListener.onTimeSelected(hourOfDay, minute);
        }
    }

    interface OnTimeSelectedListener {
        void onTimeSelected(int hourOfDay, int minute);
    }
}
