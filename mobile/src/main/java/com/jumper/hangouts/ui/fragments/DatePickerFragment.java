package com.jumper.hangouts.ui.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

import com.jumper.hangouts.R;

import java.util.Calendar;

/**
 * Created on 02/01/16.
 *
 * @author Meeth D Jain
 */
public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    public static final String ARG_DAY = "arg_day";
    public static final String ARG_MONTH = "arg_month";
    public static final String ARG_YEAR = "arg_year";


    private OnDateSelectedListener mOnDateSelectedListener;

    public static String getMyTag() {
        return DatePickerFragment.class.getSimpleName();
    }

    public static DatePickerFragment newInstance(Bundle bundle, OnDateSelectedListener onDateSelectedListener) {
        DatePickerFragment datePickerFragment = new DatePickerFragment();
        datePickerFragment.mOnDateSelectedListener = onDateSelectedListener;
        datePickerFragment.setArguments(bundle);
        return datePickerFragment;
    }

    public static Bundle getBundle(int year, int month, int day) {
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_YEAR, year);
        bundle.putInt(ARG_MONTH, month);
        bundle.putInt(ARG_DAY, day);
        return bundle;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current date as the default date in the picker
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        Bundle bundle = getArguments();
        if (bundle != null) {
            year = bundle.getInt(ARG_YEAR, year);
            month = bundle.getInt(ARG_MONTH, month);
            day = bundle.getInt(ARG_DAY, day);
        }

        return new DatePickerDialog(getActivity(), R.style.DialogTheme, this, year, month, day);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int day) {
        if (mOnDateSelectedListener != null) {
            //Call onDateSelected if listener is not null.
            mOnDateSelectedListener.onDateSelected(year, month, day);
        }
    }

    interface OnDateSelectedListener {
        void onDateSelected(int year, int month, int day);
    }
}
