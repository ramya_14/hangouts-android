package com.jumper.hangouts.ui.activities;

import android.os.Bundle;

import com.jumper.hangouts.R;

public class SplashActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
    }
}
