package com.jumper.hangouts.ui.custom;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;

/**
 * Created on 01/01/16.
 *
 * @author Meeth D Jain
 */
public class MyButton extends Button {

    public MyButton(Context context) {
        super(context, null);
    }
}
