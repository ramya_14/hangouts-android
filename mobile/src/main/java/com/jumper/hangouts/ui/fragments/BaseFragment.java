package com.jumper.hangouts.ui.fragments;

import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.jumper.hangouts.app.BusProvider;

/**
 * Created on 01/01/16.
 *
 * @author Meeth D Jain
 */
public abstract class BaseFragment extends Fragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //For inflating fragment specific menu items.
        setHasOptionsMenu(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        BusProvider.getUiBus().register(this);
    }

    @Override
    public void onPause() {
        BusProvider.getUiBus().unregister(this);
        super.onPause();
    }

    protected void setupToolbar(Toolbar toolbar, @StringRes int title, boolean showHomeAsUp, boolean homeEnabled, @DrawableRes int resId) {
        toolbar.setTitle(title);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(showHomeAsUp);
            actionBar.setHomeButtonEnabled(homeEnabled);
            actionBar.setHomeAsUpIndicator(resId);
        }
    }
}
