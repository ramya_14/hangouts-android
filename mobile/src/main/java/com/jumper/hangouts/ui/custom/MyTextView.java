package com.jumper.hangouts.ui.custom;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created on 01/01/16.
 *
 * @author Meeth D Jain
 */
public class MyTextView extends TextView {

    public MyTextView(Context context) {
        super(context, null);
    }
}
