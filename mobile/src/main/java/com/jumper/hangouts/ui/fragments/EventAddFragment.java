package com.jumper.hangouts.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.jumper.hangouts.R;
import com.jumper.hangouts.util.DateUtil;

import java.util.Calendar;
import java.util.Date;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created on 25/01/16.
 *
 * @author Meeth D Jain
 */
public class EventAddFragment extends BaseFragment implements DatePickerFragment.OnDateSelectedListener, TimePickerFragment.OnTimeSelectedListener {

    @Bind(R.id.btn_event_date_time)
    Button mEventDateTimeBtn;
    @Bind(R.id.toolbar)
    Toolbar mToolbar;

    private Calendar mCalendar;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment EventAddFragment.
     */
    public static EventAddFragment newInstance() {
        return new EventAddFragment();
    }

    public static String getMyTag() {
        return EventAddFragment.class.getSimpleName();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mCalendar = Calendar.getInstance();
        mCalendar.setTime(DateUtil.getDateRoundedToMin(30));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_event_add, container, false);
        ButterKnife.bind(this, rootView);
        setupToolbar(mToolbar, R.string.title_fragment_event_add, true, true, R.drawable.ic_toolbar_close);

        mEventDateTimeBtn.setText(DateUtil.getFormattedDateTime(mCalendar.getTime()));
        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.event_add, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home : {
                getActivity().onBackPressed();
                return true;
            }
            case R.id.action_event_create : {
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @SuppressWarnings("unused")
    @OnClick({R.id.btn_event_date_time})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_event_date_time : {
                Bundle bundle = DatePickerFragment.getBundle(mCalendar.get(Calendar.YEAR),
                        mCalendar.get(Calendar.MONTH),
                        mCalendar.get(Calendar.DAY_OF_MONTH));
                DatePickerFragment.newInstance(bundle, this).show(getFragmentManager(), DatePickerFragment.getMyTag());
                break;
            }
        }
    }

    @Override
    public void onDateSelected(int year, int month, int day) {
        mCalendar.set(year, month, day);
        Bundle bundle = TimePickerFragment.getBundle(mCalendar.get(Calendar.HOUR_OF_DAY),
                mCalendar.get(Calendar.MINUTE),
                true);
        TimePickerFragment.newInstance(bundle, this).show(getFragmentManager(), TimePickerFragment.getMyTag());
    }

    @Override
    public void onTimeSelected(int hourOfDay, int minute) {
        mCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
        mCalendar.set(Calendar.MINUTE, minute);
        //Set the selected date time value.
        mEventDateTimeBtn.setText(DateUtil.getFormattedDateTime(mCalendar.getTime()));
    }
}
