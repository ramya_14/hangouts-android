package com.jumper.hangouts.ui.activities;

import android.os.Bundle;

import com.jumper.hangouts.R;
import com.jumper.hangouts.ui.fragments.EventAddFragment;

/**
 * Created on 25/01/16.
 *
 * @author Meeth D Jain
 */
public class EventsActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_events);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, EventAddFragment.newInstance(), EventAddFragment.getMyTag())
                    .commit();
        }
    }
}
