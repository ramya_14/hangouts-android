package com.jumper.hangouts.model.rest;

import com.jumper.hangouts.model.rest.services.RestService;

/**
 * Created on 03/02/16.
 *
 * @author Meeth D Jain
 */
public class ApiManager extends RestAdapter {

    private static ApiManager ourInstance = new ApiManager();
    private RestService mRestService;

    public static ApiManager getInstance() {
        return ourInstance;
    }

    private ApiManager() {
    }
}
