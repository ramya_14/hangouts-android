package com.jumper.hangouts.app;

import android.app.Application;
import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;

/**
 * Created on 01/01/16.
 * @author Meeth D Jain
 */
public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        init();
    }

    private void init() {
        AppManager.getInstance().setApplicationContext(this);
        Fabric.with(this, new Crashlytics());
    }
}
