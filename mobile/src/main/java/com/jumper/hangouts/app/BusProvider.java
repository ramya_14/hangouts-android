package com.jumper.hangouts.app;

import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;

/**
 * Created on 02/01/16.
 *
 * @author Meeth D Jain
 */
public class BusProvider {

    private BusProvider() {}

    private static final Bus REST_BUS = new Bus(ThreadEnforcer.ANY);
    private static final Bus UI_BUS = new Bus();

    public static Bus getRestBus() {
        return REST_BUS;
    }

    public static Bus getUiBus() {
        return UI_BUS;
    }
}
