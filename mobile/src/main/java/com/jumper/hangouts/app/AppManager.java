package com.jumper.hangouts.app;

import android.app.Application;
import android.content.Context;


/**
 * Created on 01/01/16.
 *
 * @author Meeth D Jain
 */
public class AppManager {

    private static AppManager ourInstance = new AppManager();

    private Context applicationContext;

    public static AppManager getInstance() {
        return ourInstance;
    }

    private AppManager() {
    }

    public Context getApplicationContext() {
        return applicationContext;
    }

    public void setApplicationContext(Application applicationContext) {
        this.applicationContext = applicationContext;
    }
}
