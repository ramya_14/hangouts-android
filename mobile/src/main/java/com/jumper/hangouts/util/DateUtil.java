package com.jumper.hangouts.util;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created on 28/01/16.
 *
 * @author Meeth D Jain
 */
public class DateUtil {

    private DateUtil() {
    }

    public static Date getDate(long millis) {
        return new Date(millis);
    }

    public static Date getDateRoundedToMin(int roundOffInMin) {
        Calendar calendar = Calendar.getInstance();
        int min = calendar.get(Calendar.MINUTE);
        int rem = min % roundOffInMin;
        calendar.add(Calendar.MINUTE, rem != 0 ? roundOffInMin - rem : 0);
        return calendar.getTime();
    }

    public static String getFormattedDate(Date date) {
        return DateFormat.getDateInstance(DateFormat.LONG, Locale.ENGLISH).format(date);
    }

    public static String getFormattedDateTime(Date date) {
        return DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.SHORT, Locale.ENGLISH).format(date);
    }

    public static String getFormattedTime(Date date) {
        return DateFormat.getTimeInstance(DateFormat.SHORT, Locale.ENGLISH).format(date);
    }
}
