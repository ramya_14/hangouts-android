package com.jumper.hangouts.components.constants;

import com.jumper.hangouts.BuildConfig;

/**
 * Created on 02/01/16.
 *
 * @author Meeth D Jain
 */
public interface IntentConstants {

    String ACTION_START_CORE = BuildConfig.APPLICATION_ID + ".START_CORE";
}
