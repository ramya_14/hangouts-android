package com.jumper.hangouts.components.preferences;

import com.jumper.hangouts.app.AppManager;

/**
 * Created on 02/01/16.
 *
 * @author Meeth D Jain
 */
public class SessionPreference extends BasePreference {

    private static SessionPreference ourInstance = new SessionPreference();

    public static SessionPreference getInstance() {
        return ourInstance;
    }

    private SessionPreference() {
        super(AppManager.getInstance().getApplicationContext(), PreferenceType.SESSION);
    }
}
