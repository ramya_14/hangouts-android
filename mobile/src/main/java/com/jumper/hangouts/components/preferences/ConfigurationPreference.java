package com.jumper.hangouts.components.preferences;

import com.jumper.hangouts.app.AppManager;

/**
 * Created on 01/01/16.
 *
 * @author Meeth D Jain
 */
public class ConfigurationPreference extends BasePreference {

    private static ConfigurationPreference ourInstance = new ConfigurationPreference();

    public static ConfigurationPreference getInstance() {
        return ourInstance;
    }

    private ConfigurationPreference() {
        super(AppManager.getInstance().getApplicationContext(), PreferenceType.CONFIGURATION);
    }

    @Override
    public synchronized void clear() {
        //Overridden to prevent clear.
    }
}
