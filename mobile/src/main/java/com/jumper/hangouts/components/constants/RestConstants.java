package com.jumper.hangouts.components.constants;

/**
 * Created on 02/01/16.
 *
 * @author Meeth D Jain
 */
public interface RestConstants {

    String HEADER_VERSION_CODE = "version_code";
    String HEADER_VERSION_NAME = "version_name";
}
