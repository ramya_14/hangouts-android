package com.jumper.hangouts.components.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.jumper.hangouts.components.constants.IntentConstants;

/**
 * Created on 01/01/16.
 *
 * @author Meeth D Jain
 */
public class CoreService extends Service {

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if (intent != null && intent.getAction() != null) {
            String action = intent.getAction();

            if (IntentConstants.ACTION_START_CORE.equals(action)) {

            }

        } else {
            stopSelf();
        }

        return START_REDELIVER_INTENT;
    }
}
